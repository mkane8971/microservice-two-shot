from django.db import models


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name= models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()

    def __str__(self):
        return f"{self.closet_name} - {self.bin_number}/{self.bin_size}"

class Shoes(models.Model):
    model_name = models.CharField(max_length=100)
    manufacturer = models.CharField(max_length=100)
    color = models.CharField(max_length= 100)
    picture_url = models.URLField()
    bin_href = models.ForeignKey(
        BinVO,
        related_name = "shoes",
        on_delete= models.CASCADE,
        )

    def __str__(self):
        return self.model_name
# Create your models here.
