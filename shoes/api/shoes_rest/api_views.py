from django.views.decorators.http import require_http_methods
from .models import Shoes, BinVO
from django.http import JsonResponse
from common.json import ModelEncoder
import json


class BinVODetailEncoder(ModelEncoder):
    model= BinVO
    properties = ["bin_number", "import_href"]


class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties= [
        "id",
        "model_name",
        "manufacturer",
        "color",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {
            "closet_name": o.bin_href.closet_name,
            "bin_id": o.bin_href.id,
            "bin_number": o.bin_href.bin_number,
            "bin_size": o.bin_href.bin_size
            }



class ShoeDetailEncoder(ModelEncoder):
    model= Shoes
    properties = [
        "model_name",
        "manufacturer",
        "color",
        "picture_url",
    ]

    encoders = {
        "bin_href" : BinVODetailEncoder(),
    }
@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
        )
    else: #POST
        content = json.loads(request.body)
        try:
            bin_href= content["bin_href"]
            # bin_href = f"/api/conferences/{bin_vo_id}/"
            # print('Content:', bin_href)
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin_href"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin id"},
                status=400,
            )

        shoe = Shoes.objects.create(**content)
        print("\n\n\n\n\n", shoe.model_name, shoe.bin_href, shoe.manufacturer, "\n\n\n\n\n")
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_show_shoe(request,id):
    if request.method =="GET":
        shoe= Shoes.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count,_ = Shoes.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count >0 })
        # content = json.loads(request.body)
        # try:
        #     bin = BinVO.objects.get(id=content["bin"])
        #     content["bin"] = bin
        # except BinVO.DoesNotExist:
        #     return JsonResponse(
        #         {"message": "Invalid Bin id"},
        #         status= 400,
        #     )

        # shoe = Shoes.objects.create(**content)
        # return JsonResponse(
        #     shoe,
        #     encoder=ShoeDetailEncoder,
        #     safe=False
        # )
