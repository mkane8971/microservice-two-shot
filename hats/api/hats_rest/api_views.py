from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Hats, LocationVO
from django.http import JsonResponse
from common.json import ModelEncoder
import json


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]


class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "style_name",
        "fabric",
        "color",
        "picture_url",
    ]
    def get_extra_data(self, o):
        return {
            "closet_name": o.location.closet_name,
            "Section": o.location.section_number,
            "shelf_number": o.location.shelf_number
    }


class HatListEncoder(ModelEncoder):
    model = Hats,
    properties = [
        "id",
        "style_name",
        "fabric",
        "color",
        "picture_url",
        ]

    def get_extra_data(self, o):
        return {
            "closet_name": o.location.closet_name,
            "section_number": o.location.section_number,
            "shelf_number": o.location.shelf_number,
        }


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else: #POST
        content = json.loads(request.body)

        try:
            location = content["location"]
            loc = LocationVO.objects.get(import_href=location)
            content["location"] = loc

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location ID"},
                status=400,
            )

        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_show_hat(request, id):
    if request.method == "GET":
        hat = Hats.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hats.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
