from django.db import models
from django.urls import reverse

# Create your models here.


class LocationVO(models.Model):
    import_href = models.CharField(max_length=250, unique=True)
    closet_name = models.CharField(max_length=150)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return f"{self.closet_name}-{self.section_number}-{self.shelf_number}"



class Hats(models.Model):
    style_name = models.CharField(max_length=150)
    fabric = models.CharField(max_length=150)
    color = models.CharField(max_length=150)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete=models.CASCADE
        )

    def __str__(self):
             return self.style_name
