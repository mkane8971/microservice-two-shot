# Wardrobify

Team:

* Matt Kane- Shoes
* Mark Giampa - Hats

## Design

## Shoes microservice

Shoe model describes an entity that has a many to one relationship with BinVO. There can be many shoes in one BinVO but only one BinVO per shoe.
BinVO uses poller file to pull instances of Bins into the shoe microservice.
The api_list_shoes view handles the GET and POST requests to the shoe api.
React renders the applications as a SPA using JSX.

## Hats microservice
