import React, { useEffect, useState } from 'react';


function HatsForm() {
    const [style_name, setStyle] = useState('');
    const handleStyleChange = (event) => {
        const value = event.target.value;
        setStyle(value);
    }
    const [fabric, setFabric] = useState('');
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }
    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const [picture_url, setPic] = useState('');
    const handlePicChange = (event) => {
        const value = event.target.value;
        setPic(value);
    }
    const [location, setLoc] = useState('');
    const handleLocChange = (event) => {
        const value = event.target.value;
        setLoc(value);
    }
    // const [section, setSec] = useState('');
    // const handleSecChange = (event) => {
    //     const value = event.target.value;
    //     setSec(value);
    // }
    // const [shelf, setShelf] = useState('');
    // const handleShelfChange = (event) => {
    //     const value = event.target.value;
    //     setShelf(value);
    // }
    const [locations, setLocation] = useState([]);
    const fetchData = async () => {
      const url = 'http://localhost:8100/api/locations/';
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setLocation(data.locations)
      }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
        style_name,
        fabric,
        color,
        picture_url,
        location,
        // section,
        // shelf,
        };

        // console.log(data);
        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatsUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            // console.log(newHat);
            setStyle('');
            setFabric('');
            setColor('');
            setPic('');
            setLoc('');
            // setSec('');
            // setShelf('');
            }
        }


    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create A New Hat</h1>
              <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                <input onChange={handleStyleChange} placeholder="style name" required type="text" name="style name" value={style_name} id="style name" className="form-control" />
                  <label htmlFor="style name">Style Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" value={fabric} id="fabric" className="form-control"/>
                  <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" value={color} id="color" className="form-control"/>
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePicChange} placeholder="picture url" required type="url" name="picture url" value={picture_url} id="picture url" className="form-control"/>
                  <label htmlFor="picture url">Picture URL</label>
                </div>
                <div className="mb-3">
                <select onChange={handleLocChange} required name="location" value={location} id="location" className="form-select">
                  <option value="">Choose A Closet</option>
                  {locations.map(location => {
                    return ([
                        <option key={location.id} value={location.href}>{location.closet_name}</option>,
                    ]);
                  })}
                  </select>
                </div>
                {/* <div className="form-floating mb-3">
                  <input onChange={handleSecChange} placeholder="section" required type="number" name="section" value={section_number} id="section" className="form-control"/>
                  <label htmlFor="section">Choose A Section</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleShelfChange} placeholder="shelf" required type="number" name="shelf" value={shelf_number} id="shelf" className="form-control"/>
                  <label htmlFor="shelf">Choose A Shelf</label>
                </div> */}
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }

    export default HatsForm;
