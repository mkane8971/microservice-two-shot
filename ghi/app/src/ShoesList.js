import { useEffect, useState } from 'react';

function ShoesList(props) {
    const [shoes, setShoes] = useState([])

    const getData = async() => {
        const response= await fetch('http://localhost:8080/api/shoes/');

        if(response.ok) {
            const data = await response.json();
            setShoes(data.shoes)
        }
    }

    const handleDelete = async (id) =>{
        const url = `http://localhost:8080/api/shoes/${id}`;
        const fetchConfig= {
            method: "DELETE",
            headers: { "Content-Type": "application/json" },
        };
        await fetch(url, fetchConfig);
        getData();
    }
    useEffect(()=>{
        getData()
    }, [])

    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Manufacturer</th>
                <th>Model</th>
                <th>Color</th>
                <th>Photo</th>
                <th>Bin ID</th>
                <th>Closet</th>
                <th>Bin Number</th>
                <th>Bin Size</th>
            </tr>
            </thead>
            <tbody>
            {shoes.map(shoe => {
                const key = shoe.id;
                console.log(shoe)
                return (
                <tr key={key}>
                    <td>{ shoe.model_name }</td>
                    <td>
                        <img src={ shoe.picture_url } alt="" style={{height:90, width:90}}></img>
                    </td>
                    <td>{ shoe.manufacturer }</td>
                    <td>{ shoe.color }</td>
                    <td>{ shoe.bin_id }</td>
                    <td>{shoe.closet_name}</td>
                    <td> #{shoe.bin_number} </td>
                    <td> {shoe.bin_size} foot</td>

                    <td>
                        <button
                        onClick={() => handleDelete(shoe.id, props.getShoe)}>Delete</button>
                    </td>

                </tr>
                );
            })}
            </tbody>
        </table>
        );
    }

export default ShoesList;
