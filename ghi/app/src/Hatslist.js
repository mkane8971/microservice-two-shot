import { useEffect, useState } from 'react';


function HatsList() {
    const [hats, sethats] = useState([]);
    const [deleteItem, setDeleteItem] = useState(null);

    const getData = async() => {
        const response= await fetch('http://localhost:8090/api/hats/');

        if(response.ok) {
            const data = await response.json();
            sethats(data.hats)
        }
    }

    const deleteHat = async (id) => {
        const url = `http://localhost:8090/api/hats/${id}`;
        const fetchConfig = {
            method: "DELETE",
            headers: { "Content-Type": "application/json" },
        };
        await fetch(url, fetchConfig);
        getData();
        setDeleteItem(null);
    };

    const handleDeleteConfirmation = (id) => {
        setDeleteItem(id);
    };


    useEffect(()=>{
        getData()
    }, [])


    return (
    <table className="table table-striped">
          <thead>
        <tr>
            <th>Style Name</th>
            <th>Photo</th>
            <th>Fabric</th>
            <th>Color</th>
            <th>Location</th>
            <th>Section</th>
            <th>Shelf</th>
            <th>Delete</th>

        </tr>
        </thead>
        <tbody>
        {hats.map(hat => {
            const key = hat.id;
            return (
            <tr key={key}>
                <td>{ hat.style_name }</td>
                <td>
                    <img src={ hat.picture_url } alt="" style={{height:90, width:90}} ></img>
                </td>
                <td>{ hat.fabric }</td>
                <td>{ hat.color} </td>
                <td>{ hat.closet_name} </td>
                <td>{ hat.section_number} </td>
                <td>{ hat.shelf_number} </td>
                <td>
                    <button onClick={() => handleDeleteConfirmation(hat.id)}>Delete</button>
                    {deleteItem === hat.id && (
                    <div>
                        Are you sure?
                        <button onClick={() => deleteHat(hat.id)}>Yes</button>
                        <button onClick={() => setDeleteItem(null)}>No</button>
                    </div>
                    )}
                </td>
            </tr>
            );
        })}
        </tbody>
  </table>
    );
}

export default HatsList;
